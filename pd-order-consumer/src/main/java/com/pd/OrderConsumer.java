package com.pd;

import com.pd.pojo.PdOrder;
import com.pd.service.OrderService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*
springboot 整合rabbitmq 后，
消费者不用写代码，通过注解配置，
就可以接受消息、消息转换
 */
@Component
@RabbitListener(queues = "orderQueue")
public class OrderConsumer {
    @Autowired
    private OrderService orderService;

    @RabbitHandler //指定处理消息的方法，只能有一个
    public void receive(PdOrder pdOrder) throws Exception {
        orderService.saveOrder(pdOrder);
        System.out.println("===========订单已存储============");
    }
}
